<h1 align="center"> <img src="source/assets/icons/icon32.png" alt="zd-dl-router icon" /> zd-dl-router</h1>
<p align="center">A cross browser extension that routes downloads from Zendesk into separate folders</p>
<h3 align="center">Made by <a href="https://gitlab.com/">GitLab Inc.</a></h3>
<hr />

## Features

- Cross Browser Support (Web-Extensions API)
- Routes tickets into separate folders based on ticket ID, ticket title and ticket organization
- Easily open previously downloaded files when viewing a ticket by clicking on the extension icon (via the extension icon and a keyboard shortcut)
- Download all attachments at once in the current ticket (via the ticket dropdown or via a keyboard shortcut)
- Download all attachments at once for each ticket comment

### Customising the download path

> **_NOTE:_**  The WebExtension API only allows saving into directories relative to the default download directory. Symlinks can be used to get around this limitation:
>
> Windows: `mklink /D C:\path\to\symlink D:\path\to\actual`
>
> Mac/Unix: `ln -s /path/to/actual /path/to/symlink`

The download path can be customized in the extension options:

![options](images/options.png)

### Customising the keyboard shortcuts

The extension supports user defined keyboard shortcuts for the following actions:

- Downloads all attachments for the current ticket
- Opens the download folder for the current ticket (if it exists)

Out of the box, there are no keyboard shortcuts defined by default and these can be set in your browser:

- Chrome: `chrome://extensions/shortcuts`
- Firefox: [Manage extension shortcuts in Firefox](https://support.mozilla.org/en-US/kb/manage-extension-shortcuts-firefox)

### Limitations

Due to the challenges of working with different extension APIs for different vendors, the extension currently
has the following known limitations that cannot be fixed:

- (Firefox) Downloading images by clicking the "Download" button inside a modal view won't be routed and the image
will be saved in the default download folder instead. This is due to a [known bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1696174)
in Firefox where blobs cannot be downloaded via the `browser.downloads.download` API.
- (Firefox) The initial download is cancelled before a new download is started, and the new download is routed
appropriately. Most users won't notice this, but the Downloads window will show cancelled downloads alongside
the successful one. This is due to a [known bug](https://bugzilla.mozilla.org/show_bug.cgi?id=1245652) in Firefox
where the `browser.downloads.onDeterminingFilename` API has not been implemented, and we need to resort to using
the `browser.downloads.onCreated` and `browser.downloads.download` APIs as a workaround. Once Mozilla implements
the missing API we can remove the Firefox specific code from the extension.
- Any Zendesk attachments that are downloaded via an email message from Zendesk will not be routed. This is because
it is impossible to determine which ticket the attachment belongs to without writing DOM parsers for each web
email client.

## Screenshots

Does your download folder look like this?

<img src="images/before.png" alt="before" style="zoom:50%;" />

Make it look like this:

<img src="images/after.png" alt="after" style="zoom: 50%;" />

## Browser Support and install

| [![Chrome](https://raw.github.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png)](/) | [![Firefox](https://raw.github.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png)](/) | [![Opera](https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png)](/) | [![Edge](https://raw.github.com/alrra/browser-logos/master/src/edge/edge_48x48.png)](/) | [![Brave](https://raw.github.com/alrra/browser-logos/master/src/brave/brave_48x48.png)](/) |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 49 & later ✔ | 52 & later ✔ | 36 & later ✔ | 79 & later ✔ | Latest ✔ | Latest ✔ | Latest ✔
| [Install](https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml) | [Install](https://addons.mozilla.org/en-GB/firefox/addon/zendesk-download-router/) | [Install](https://addons.opera.com/en-gb/extensions/details/zendesk-download-router/) | [Install](https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml)<sup>1</sup> | [Install](https://chrome.google.com/webstore/detail/zendesk-download-router/pgfhacdbkdeppdjgighdeejjfneifkml) |

1. Using the Chrome extension, and when **Allow extensions from other stores** is enabled. See [To add an extension to Microsoft Edge from the Chrome Web Store](https://support.microsoft.com/en-nz/help/4538971/microsoft-edge-add-or-remove-extensions) on the Microsoft Edge Support website for more information.

## Cleaning up the downloads

In order to avoid cluttering up your downloads folder,
regularly run [the `zd-dl-wiper`](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-wiper).

Another option is using a tool like [Hazel](https://www.noodlesoft.com/) with rules like these:

![](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/uploads/459a0790f77fde85f0f04cb6c78b4e3a/hazel-archive_old_ZDs.png)

![](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/uploads/64c5720ba93ca5de02c16ec627ff5bb3/hazel-delete-old-ZD-archives.png)

## 🚀 Prerequsites for development

Ensure you have:

- [Node.js](https://nodejs.org) v14 or v16 installed (build tasks won't work on v18)
- [Yarn](https://yarnpkg.com) v1 or v2 installed

Then run the following:

- `yarn install` to install dependencies.
- `yarn run dev:chrome` to start the development server for chrome extension
- `yarn run dev:firefox` to start the development server for firefox addon
- `yarn run dev:opera` to start the development server for opera extension
- `yarn run test:eslint:check` to run a lint check on javascript source files
- `yarn run test:eslint:fix` to fix linter errors on javascript source files
- `yarn run test:prettier:check` to run a lint check on CSS and HTML source files
- `yarn run test:prettier:fix` to fix linter errors on CSS and HTML source files
- `yarn run build:chrome` to build Chrome extension
- `yarn run build:firefox` to build Firefox addon
- `yarn run build:opera` to build Opera extension
- `yarn run build:chrome` to build Chrome extension
- `yarn run build:firefox` to build Firefox addon
- `yarn run deploy:chrome` to deploy Chrome extension (`yarn run build:chrome` should be executed first)
- `yarn run deploy:firefox` to deploy Firefox addon (`yarn run build:firefox` should be executed first)
- `yarn run build` builds and packs extensions all at once to `extension/` directory

### Development

- `yarn install` to install dependencies.
- To watch file changes in developement

  - Chrome
    - `yarn run dev:chrome`
  - Firefox
    - `yarn run dev:firefox`
  - Opera
    - `yarn run dev:opera`

- **Load extension in browser**

  - ### Chrome

    - Go to the browser address bar and type `chrome://extensions`
    - Check the `Developer Mode` button to enable it.
    - Click on the `Load Unpacked Extension…` button.
    - Select your extension’s extracted directory (`extension/chrome`).

  - ### Firefox

    - Load the Add-on via `about:debugging` as temporary Add-on.
    - Choose the `manifest.json` file in the extracted directory (`extension/firefox`).

  - ### Opera

    - Load the extension via `opera:extensions`
    - Check the `Developer Mode` and load as unpacked from extension’s extracted directory (`extension/opera`).
   
### Generating browser specific manifest.json
Update `src/manifest.json` file with browser vendor prefixed manifest keys

```js
{
  "__chrome__name": "SuperChrome",
  "__firefox__name": "SuperFox",
  "__edge__name": "SuperEdge",
  "__opera__name": "SuperOpera"
}
```

if the vendor is `chrome` this compiles to:

```js
{
  "name": "SuperChrome",
}
```

---

Add keys to multiple vendors by seperating them with | in the prefix

```js
{
  __chrome|opera__name: "SuperBlink"
}
```

if the vendor is `chrome` or `opera`, this compiles to:

```js
{
  "name": "SuperBlink"
}
```

See the original [README](https://github.com/abhijithvijayan/wext-manifest-loader) of `wext-manifest-loader` package for more details

### Production

- `yarn run build` builds the extension for all the browsers to `extension/BROWSER` directory respectively.

### Publishing to browser extension stores

> **_NOTE:_**  Follow [semantic versioning](https://semver.org/) when publishing a new version. 

1. Increment the version in `package.json`, commit the changes and merge them with the `master` branch.
1. [Push a new tag](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/-/tags/new) (using the new version as the tag name).
1. Merge the change into the `master` branch. A manual job to deploy the extensions for Chrome/Firefox will appear in the pipeline.
Start the deploy jobs manually for the extensions to be published to the respective store.

## Troubleshooting

### When I download a file, I get an HTML file instead containing `access-unauthenticated` in the body

The extension requires access to your Zendesk cookies in order to authenticate and allow downloading of Zendesk attachments.

If your Zendesk instance has [private attachments](https://support.zendesk.com/hc/en-us/articles/204265396-Enabling-attachments-in-tickets#topic_nrp_bnx_xdb) 
enabled, this can be an issue.

If you are using Firefox, ensure that first-party isolation is set to `false` under `about:config` and search for `privacy.firstparty.isolate`.

For this same reason, if you are using [Firefox Multi-Account Containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/), be sure that your Zendesk session is _not_ in a container, so that the zd-dl-router extension can access your Zendesk cookies.

### A download is not routed to where I expect

Due to the way that the Zendesk agent application asynchronously loads data into the DOM, it can lead to race conditions
where the extension cannot extract the ticket information used to route downloads before an attachment is routed. When this
happens, any attachments won't be routed and they will be downloaded to the browser's default folder. If this happens,
refreshing the page and then attempting to download the attachment again will result in the extension routing the download
to the expected location.

Please ensure you are using the latest version of your browser and extension.

If there are no [open issues](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/-/issues) that match your problem,
please open a new issue with the following information:

- Your browser name and version
- The Zendesk ticket URL and attachment name you are trying to download
- Your routing configuration ie. where do you expect the download to end up?
- Any errors you might have seen in the **Console** tab of your browser's **Developer Tools**
- Reproduction steps. This is important so I can help you resolve the problem quickly.

## Show your support

Give a ⭐️ if this project helped you!

## Licence

Code released under the [MIT License](LICENSE).
