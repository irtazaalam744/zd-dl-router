import browser from 'webextension-polyfill';
import {debugMessage, printDebugInformation} from './utils.js';
import '../styles/contentScript.scss';

require('log-timestamp')('[zd-dl-router]');

const dlButtonsAddedClass = 'zdr-done';

function downloadAttachmentsForComment(e) {
  e.preventDefault();

  let commentId = null;

  if (e.currentTarget.hasAttribute('test-comment-id')) {
    commentId = e.currentTarget.getAttribute('test-comment-id');
    debugMessage(
      `Downloading attachments for comment ID ${commentId} in ${window.location.href}`
    );
  } else {
    debugMessage(`Downloading all attachments for ${window.location.href}`);
  }

  browser.runtime.sendMessage({
    action: 'download-attachments-for-comment',
    commentId,
  });
}

/**
 * Once the Zendesk ticket is loaded, extract the necessary fields and send
 * them back to the background script
 */
function extractTicketFields() {
  debugMessage(`Extracting ticket data for ${window.location.href}`);

  const header = document.getElementById('branding_header');
  const mainPanes = document.getElementById('main_panes');
  const activePane = mainPanes.querySelector(
    ':scope > div.workspace:not([style*="opacity: 0"])'
  );
  const payload = {};

  const title = header.querySelector(
    'div[data-selected="true"] div[data-test-id="header-tab-title"]'
  );
  if (title != null) {
    payload.ticketTitle = title.innerHTML;
  }

  const id = header.querySelector('div[data-selected="true"]');
  if (id != null && id.hasAttribute('data-entity-id')) {
    payload.ticketId = id.getAttribute('data-entity-id');
  } else {
    // Grab the ticket ID from the window href if we cannot find it in the DOM
    let url = window.location.href;
    url = url.split('/').pop();
    payload.ticketId = url;
  }

  const orgName = activePane.querySelector('span.organization-pill');
  if (orgName != null) {
    payload.ticketOrgName = orgName.innerText;
  }

  const attachments = activePane.querySelectorAll(
    '[data-test-id=attachment-thumbnails-container] a'
  );
  if (attachments != null) {
    const attachmentUrls = [];
    for (const attachment of attachments) {
      const content = attachment.closest('div.content');

      // TODO: Find another way to group attachments
      // Break the loop as we don't have a workaround at the moment
      // eslint-disable-next-line no-continue
      if (content === null) continue;

      const commentId = content.getAttribute('data-comment-id');
      const uploadedOn = content
        .querySelector('div.header time')
        .getAttribute('datetime');
      attachmentUrls.push({
        commentId,
        uploadedOn,
        url: attachment.getAttribute('href'),
      });
    }
    payload.ticketAttachments = attachmentUrls;
  }

  // HOTFIX: When the window is refocused, a change event is not generated
  // Due to a bug where the "ticketDownloadFolder" isn't set sometimes, this
  // means that variable will not be set if the URL doesn't change. Let's
  // force the payload to change each time so the variable can be recalculated
  // if needed
  payload.dateGenerated = Date.now();

  browser.storage.local.set({ticketMetadata: payload}).then(() => {
    debugMessage(`Caching ticket details for ${window.location.href}`);
    printDebugInformation();
  });
}

/**
 * Adds download buttons to the active ticket
 */
function addDownloadButtons() {
  const mainPanes = document.getElementById('main_panes');
  const activePane = mainPanes.querySelector(
    ':scope > div.workspace:not([style*="opacity: 0"])'
  );

  // Add the download button for each comment
  const attachmentContainers = activePane.querySelectorAll(
    'div[data-test-id=attachment-thumbnails-container]:not(.zdr-done)'
  );

  for (const container of attachmentContainers) {
    const attachmentCount = container.childElementCount;

    // Skip comments that have one attachment
    // eslint-disable-next-line no-continue
    if (attachmentCount <= 1) continue;

    let commentId = container.closest('div.content');

    // TODO: Find another way to group attachments
    // Break the loop as we don't have a workaround at the moment
    if (commentId === null) break;

    commentId = commentId.getAttribute('data-comment-id');

    const element = Object.assign(document.createElement('div'), {
      className: 'fYTOSH',
      innerHTML: '<button class="cXYbMy zdr-dl-btn"></button>',
    });

    const button = element.firstChild;
    button.setAttribute('test-comment-id', commentId);
    button.classList.add('zdr-dl-btn');
    button.setAttribute('title', 'Download all attachments for this comment');
    button.addEventListener('click', downloadAttachmentsForComment);

    const countPill = Object.assign(document.createElement('div'), {
      className: 'zdr-dl-count',
      innerHTML: attachmentCount,
    });
    button.append(countPill);

    const image = Object.assign(document.createElement('img'), {
      className: 'ipwOHi',
      src: browser.runtime.getURL('assets/icons/icon128.png'),
      style: 'width:60px;height:60px;',
    });
    button.appendChild(image);

    container.prepend(element);
    container.classList.add(dlButtonsAddedClass);
  }

  // Add the all downloads button to the dropdown
  const ticketOptions = activePane.querySelector(
    'button[data-test-id=mast-ticket-options]:not(.zdr-done)'
  );

  if (ticketOptions != null) {
    const dropdown = ticketOptions.nextElementSibling;

    const option = document.createElement('li');
    const button = Object.assign(document.createElement('a'), {
      innerHTML: 'Download all attachments',
      className: 'zdr-dl-btn',
    });

    button.addEventListener('click', downloadAttachmentsForComment);

    option.append(button);
    dropdown.append(option);

    // Prevent multiple buttons from being added
    ticketOptions.classList.add(dlButtonsAddedClass);
  }
}

function rafAsync() {
  return new Promise((resolve) => {
    requestAnimationFrame(resolve); // faster than set time out
  });
}

function checkWorkspaceElement(workspace, selector) {
  if (workspace.querySelector(selector) === null) {
    return rafAsync().then(() => checkWorkspaceElement(workspace, selector));
  }
  return Promise.resolve(true);
}

browser.runtime.onMessage.addListener((request, _sender, _sendResponse) => {
  if (
    request.action === 'refresh' &&
    window.location.href.includes('zendesk.com/agent/tickets/')
  ) {
    // Check if elements exist on the page before attempting to extract
    // anything. The mutator observer will fire once everything is loaded
    if (document.querySelector('#main_panes div.zd-comment')) {
      debugMessage(
        `Background script requested ticket refresh | URL: ${window.location.href}`
      );
      extractTicketFields();
    } else {
      debugMessage(
        `Background script requested ticket refresh, but the DOM is not loaded yet | URL: ${window.location.href}`
      );
    }
  }
});

/**
 * Observe any changes made to the page and trigger a field extraction
 * if changes are detected
 */
const changeWorkspaceFocus = (workspace) => {
  if (!window.location.href.includes('zendesk.com/agent/tickets/')) return;

  if (workspace.tagName === 'DIV') {
    const style = workspace.getAttribute('style');
    if (!style || !style.match('.*display:\\s*none;.*')) {
      // If the workspace hasn't been cached, the ticket data could be missing
      // Wait for ticket data to be loaded before extracting the ticket fields
      checkWorkspaceElement(workspace, 'div.zd-comment').then(() => {
        // Add a delay to allow the DOM to update
        setTimeout(() => {
          // Only extract fields if the tab is visible to the user
          if (document.hasFocus() && !document.hidden) extractTicketFields();

          addDownloadButtons();
        }, 500);
      });
    }
  }
};

const workspaceWatcher = (mutations) => {
  mutations.forEach((mutation) => {
    changeWorkspaceFocus(mutation.target);
  });
};

const workspaceHook = (mutations) => {
  mutations.forEach((mutation) => {
    mutation.addedNodes.forEach((node) => {
      changeWorkspaceFocus(node);
      const observer = new MutationObserver(workspaceWatcher);
      observer.observe(node, {attributes: true, attributeFilter: ['style']});
    });
  });
};

const mutationLoader = () => {
  const mainPanes = document.getElementById('main_panes');
  if (mainPanes) {
    const observer = new MutationObserver(workspaceHook);
    observer.observe(mainPanes, {childList: true});
    mainPanes.childNodes.forEach((x) => {
      changeWorkspaceFocus(x);
    });
  } else {
    window.setTimeout(mutationLoader, 100);
  }
};

mutationLoader();

console.log('Content script initialised');
