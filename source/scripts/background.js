import browser from 'webextension-polyfill';
import {debugMessage, printDebugInformation} from './utils.js';

require('log-timestamp');

const agentBaseUrl = 'zendesk.com/agent/tickets';

/**
 * Transform a string to a safe folder name by stripping any illegal characters
 * not allowed by the filesystem
 * @param {*} folderName The folder name to process
 * @param {*} toLower If the string should be converted to lowercase
 */
function transformToSafeFolderName(folderName, toLower = false) {
  // Return an empty string if we got passed nothing
  if (!folderName) {
    return '';
  }

  let safeFolder = folderName;

  // Remove any HTML entities in the string
  safeFolder = safeFolder.replace(/&#{0,1}[a-z0-9]+;/gi, '');

  // Regex any illegal characters out of the string
  safeFolder = safeFolder.replace(/[|&;$%@".<>()+,/\\~:?]/g, '');

  // Remove any multiple spaces
  safeFolder = safeFolder.replace(/\s\s+/g, ' ');

  // Convert to lowercase if desired
  if (toLower) {
    safeFolder = safeFolder.toLowerCase();
  }

  return safeFolder;
}

/**
 * Determines the relative download path and replaces calculated variables with
 * the actual values
 * @return string
 */
function determineRelativeDownloadPath() {
  browser.storage.local
    .get([
      'ticketMetadata',
      'baseDownloadPath',
      'transformPathToLowercase',
      'activeAgentUrl',
    ])
    .then((s) => {
      // Don't update the relative path if this isn't an active agent URL
      if (!s.activeAgentUrl) return;

      let {baseDownloadPath} = s;

      // Replace variables in download path (and strip illegal characters)
      if (baseDownloadPath.includes('%TICKET_ID%')) {
        baseDownloadPath = baseDownloadPath.replace(
          '%TICKET_ID%',
          s.ticketMetadata.ticketId
        );
      }

      if (baseDownloadPath.includes('%TICKET_TITLE%')) {
        baseDownloadPath = baseDownloadPath.replace(
          '%TICKET_TITLE%',
          transformToSafeFolderName(
            s.ticketMetadata.ticketTitle,
            s.transformPathToLowercase
          )
        );
      }

      if (baseDownloadPath.includes('%TICKET_ORG%')) {
        baseDownloadPath = baseDownloadPath.replace(
          '%TICKET_ORG%',
          transformToSafeFolderName(
            s.ticketMetadata.ticketOrgName,
            s.transformPathToLowercase
          )
        );
      }

      browser.storage.local
        .set({ticketDownloadFolder: baseDownloadPath})
        .then(() => {
          printDebugInformation();
        });
    });
}

/**
 * Sends a message to the content script to refresh the ticket information
 * @param {*} tab The active tab
 */
function refreshTicketInformation(tab) {
  // Don't process if the active tab isn't a ticket
  if (!tab.url.includes(agentBaseUrl)) return;

  // This prevents sending a message before the content script is ready
  // The mutator observer will send updates when loading is done
  if (tab.status === 'complete') {
    debugMessage(
      `ACTION: refreshTicketInformation | ID: ${tab.id} URL: ${tab.url}`
    );
    browser.tabs.sendMessage(tab.id, {action: 'refresh'});
  }
}

/**
 * Shows or hides the page action depending on if the current tab is a ticket
 * or not
 * @param {*} tab The active tab
 */
function updatePageAction(tab) {
  if (tab.url.includes(agentBaseUrl)) {
    browser.pageAction.setIcon({
      tabId: tab.id,
      path: 'assets/icons/icon128.png',
    });
    browser.pageAction.show(tab.id);
  } else {
    browser.pageAction.setIcon({
      tabId: tab.id,
      path: 'assets/icons/icon-bw128.png',
    });
    browser.pageAction.hide(tab.id);
  }
}

/**
 * Determines if the active tab is a Zendesk ticket or not
 * @param {*} tab The active tab
 */
function updateCurrentTicket(tab) {
  // Skip any tabs that have no URLs
  if (!tab.url) return;

  if (tab.active && tab.url.includes(agentBaseUrl)) {
    browser.windows.get(tab.windowId).then((window) => {
      if (window.focused) {
        const ticketId = tab.url.split('/').pop();
        browser.storage.local.set({
          activeAgentUrl: true,
          activeTicketId: ticketId,
        });
      }
    });
  } else {
    browser.storage.local.set({activeAgentUrl: false});
    browser.storage.local.remove([
      'activeTicketId',
      'ticketMetadata',
      'ticketDownloadFolder',
    ]);
  }
}

/**
 * Downloads all attachments for the current ticket
 * @param {*} commentId Only download attachments belonging to the specified commentId
 * @returns
 */
function downloadAllAttachments(commentId = null) {
  browser.storage.local
    .get(['ticketMetadata', 'ticketDownloadFolder'])
    .then((s) => {
      if (s.ticketMetadata.ticketAttachments == null) return;

      for (const attachment of s.ticketMetadata.ticketAttachments) {
        // eslint-disable-next-line no-continue
        if (commentId != null && attachment.commentId !== commentId) continue;

        const filename = attachment.url
          .split('/')
          .pop()
          .replace(/^(\?name=)/, '');
        browser.downloads.download({
          filename: s.ticketDownloadFolder + filename,
          url: attachment.url,
        });
      }
    });
}

/**
 * When the extension is initialised, set any default settings up for the first time
 */
browser.storage.local
  .get({
    baseDownloadPath: 'zd-%TICKET_ID%/',
    transformPathToLowercase: false,
    debugMode: false,
  })
  .then((s) => {
    // Set defaults in storage, in case we are loading the extension for the first time
    browser.storage.local.set({
      baseDownloadPath: s.baseDownloadPath,
      transformPathToLowercase: s.transformPathToLowercase,
      debugMode: s.debugMode,
    });

    // Remove retired settings from the sync storage since we use local storage now
    browser.storage.sync.clear();
  });

/**
 * Opens the download folder for the current ticket (if it exists)
 */
function openDownloadFolder() {
  browser.storage.local.get(['ticketMetadata']).then((s) => {
    const {ticketId} = s.ticketMetadata;

    if (!ticketId) return;

    browser.downloads
      .search({filenameRegex: ticketId, exists: true, limit: 1})
      .then((results) => {
        // Only attempt to open the folder if we have results
        if (Array.isArray(results) && results.length) {
          // Use Chrome API because the polyfill library generates a warning
          chrome.downloads.show(parseInt(results[0].id));
        }
      });
  });
}

/**
 * Listen for any changes made to the extension options. If any changes are
 * made, update the internal variables.
 */
browser.storage.onChanged.addListener((changes, _area) => {
  if ('ticketMetadata' in changes) {
    debugMessage('ACTION: Storage changes detected to the ticket metadata');
    determineRelativeDownloadPath();
  }
});

// Show Parent Folder when extension icon is clicked
browser.pageAction.onClicked.addListener((_tab) => {
  openDownloadFolder();
});

// Listener for receiving the current ticket title
browser.runtime.onMessage.addListener((request, sender, _sendResponse) => {
  // Don't process if the sender tab is not active
  if (!sender.tab.active) return Promise.resolve('ok');

  // Don't process autoloader messages
  if (request.type) return Promise.resolve('ok');

  if (request.action) {
    if (request.action === 'download-attachments-for-comment') {
      downloadAllAttachments(request.commentId);
    }

    return Promise.resolve('ok');
  }

  return Promise.resolve('ok');
});

browser.tabs.onActivated.addListener((currentTab) => {
  browser.tabs.get(currentTab.tabId).then((tab) => {
    debugMessage(`ACTION: onActivated | ID: ${tab.id} URL: ${tab.url}`);

    updatePageAction(tab);
    updateCurrentTicket(tab);
    refreshTicketInformation(tab);
  });
});

browser.windows.onFocusChanged.addListener((_windowId) => {
  browser.tabs.query({active: true, currentWindow: true}).then((tabs) => {
    // Don't process if the sender window contains no tabs (developer console for example)
    if (tabs.length === 0) return;

    const tab = tabs[0];

    debugMessage(`ACTION: onFocusChanged | ID: ${tab.id} URL: ${tab.url}`);

    updatePageAction(tab);
    updateCurrentTicket(tab);
    refreshTicketInformation(tab);
  });
});

browser.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
  // Only process active tabs
  if (!tab.active) return;

  debugMessage(`ACTION: onUpdated | ID: ${tab.id} URL: ${tab.url}`);

  updatePageAction(tab);
  updateCurrentTicket(tab);
});

browser.commands.onCommand.addListener((command) => {
  browser.storage.local.get(['activeAgentUrl']).then((s) => {
    if (s.activeAgentUrl) {
      if (command === 'download-all-attachments') {
        downloadAllAttachments();
      }
      if (command === 'open-download-folder') {
        openDownloadFolder();
      }
    }
  });
});

console.log('Background script initialised');
