import browser from 'webextension-polyfill';
import '../styles/options.scss';

function flashAlert(msg, type) {
  const status = document.getElementById('status');

  status.classList = '';
  status.classList.add('alert');
  status.classList.add(type);
  status.textContent = msg;
  status.style.display = 'block';

  // Auto hide the message if a success
  if (type === 'alert-success') {
    setTimeout(() => {
      status.style.display = 'none';
    }, 1000);
  }
}

// Restores select box and checkbox state using the preferences
// stored in browser.storage.
function restoreOptions() {
  browser.storage.local
    .get(['baseDownloadPath', 'transformPathToLowercase', 'debugMode'])
    .then((items) => {
      document.getElementById('baseDownloadPath').value =
        items.baseDownloadPath;
      document.getElementById('transformPathToLowercase').checked =
        items.transformPathToLowercase;
      document.getElementById('debugMode').checked = items.debugMode;
    });
}

// Saves options to browser.storage
function saveOptions() {
  let baseDownloadPath = document.getElementById('baseDownloadPath').value;
  const transformPathToLowercase = document.getElementById(
    'transformPathToLowercase'
  ).checked;
  const debugMode = document.getElementById('debugMode').checked;

  // Add trailing slash if it doesn't exist
  baseDownloadPath += baseDownloadPath.endsWith('/') ? '' : '/';

  browser.storage.local
    .set({
      baseDownloadPath,
      transformPathToLowercase,
      debugMode,
    })
    .then(() => {
      // Update status to let user know options were saved.
      flashAlert('✔️ Options saved', 'alert-success');

      // Reload the options
      restoreOptions();
    });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.getElementById('save').addEventListener('click', saveOptions);
