import browser from 'webextension-polyfill';

const mozWhitelistedReferrers = ['zendesk.com', 'zdusercontent.com'];

function completeFirefoxDownload(inputLinks) {
  // Change the links string back into an array for processing
  const links = inputLinks[0].split(',');

  browser.storage.local
    .get(['mozDownloadPath', 'mozDownloadFilename', 'mozDownloadUrl'])
    .then((s) => {
      let url = s.mozDownloadUrl;

      // Only fix whitelisted URLs
      // If the URL comes from an external source ie. S3, we don't want to modify the URL
      if (
        !url.includes('blob:') &&
        mozWhitelistedReferrers.some((v) => url.includes(v))
      ) {
        // Get the token from the current link we have to match up
        const token = s.mozDownloadUrl.split('/')[5].split('?')[0];

        url = '';
        // Loop the links
        for (const link of links) {
          if (token === link.split('/')[5]) {
            url = link;
            break;
          }
        }
      }

      // Clear the values from local storage
      browser.storage.local.remove([
        'mozDownloadPath',
        'mozDownloadFilename',
        'mozDownloadUrl',
      ]);

      // Start a new download with the routed path
      browser.downloads.download({
        filename: s.mozDownloadPath + s.mozDownloadFilename,
        url,
      });
    });
}

// Workaround - Firefox does not support browser.downloads.onDeterminingFilename
// https://bugzilla.mozilla.org/show_bug.cgi?id=1245652
browser.downloads.onCreated.addListener((downloadItem) => {
  // HOTFIX - Firefox doesn't support starting new blob downloads, so don't route blobs
  // https://bugzilla.mozilla.org/show_bug.cgi?id=1696174
  // https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router/-/issues/25
  if (downloadItem.url.includes('blob:')) return;

  browser.storage.local
    .get(['ticketMetadata', 'ticketDownloadFolder', 'activeAgentUrl'])
    .then((s) => {
      // Only route if the active tab is a Zendesk ticket
      if (!s.activeAgentUrl) {
        return;
      }

      const downloadPath = s.ticketDownloadFolder;

      // Determine if the download has been routed already, otherwise cancel it
      // Firefox requires us to recreate the download in order to route the download
      if (!downloadItem.filename.includes(downloadPath)) {
        // Cancel the download so we can recreate it with the correct folder path
        browser.downloads.cancel(downloadItem.id);
        browser.downloads.erase({id: downloadItem.id});

        // Workaround - Firefox does not support download redirects
        // https://bugzilla.mozilla.org/show_bug.cgi?id=1254327

        // Save variables we require for the next function
        browser.storage.local.set(
          {
            mozDownloadPath: downloadPath,
            mozDownloadFilename: downloadItem.filename.split('/').pop(),
            mozDownloadUrl: downloadItem.url,
          },
          () => {
            // Fetch all attachment links in the DOM for the next function
            // The code segment must return a string
            browser.tabs.executeScript(
              {
                code: "(()=> {return Array.from(document.querySelectorAll('a[data-test-id=attachment-thumbnail]')).map(l => l.href).toString();})();",
              },
              completeFirefoxDownload
            );
          }
        );
      }
    });
});
