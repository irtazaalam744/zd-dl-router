import browser from 'webextension-polyfill';

/**
 * Prints debug information about the current data stored in the browser storage
 */
export function printDebugInformation() {
  browser.storage.local
    .get([
      'ticketMetadata',
      'ticketDownloadFolder',
      'activeAgentUrl',
      'debugMode',
    ])
    .then((s) => {
      if (s.debugMode) {
        console.log('Active Agent URL (boolean):', s.activeAgentUrl);
        console.log('Download folder (string):', s.ticketDownloadFolder);
        console.log('Ticket metadata (array):', s.ticketMetadata);
      }
    });
}

export function debugMessage(message) {
  browser.storage.local.get(['debugMode']).then((s) => {
    if (s.debugMode) console.log(message);
  });
}
