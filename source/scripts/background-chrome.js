import browser from 'webextension-polyfill';

browser.downloads.onDeterminingFilename.addListener((item, suggest) => {
  browser.storage.local
    .get(['ticketMetadata', 'ticketDownloadFolder', 'activeAgentUrl'])
    .then((s) => {
      let downloadPath = item.filename;

      // Only route if the active tab is a Zendesk ticket
      if (s.activeAgentUrl) {
        downloadPath = s.ticketDownloadFolder + item.filename;
      }

      suggest({filename: downloadPath});
    });

  // Needed for the async function to be successful
  return true;
});
